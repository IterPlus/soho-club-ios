//
//  InsertInfomationVC.swift
//  soho
//
//  Created by WML on 8/16/18.
//  Copyright © 2018 sohogroup. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD

class CreateUserVC: UIViewController {

    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var phoneTF: UITextField!
    @IBOutlet weak var genderTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var signTopCons: NSLayoutConstraint!
    var topOrigin: CGFloat = 0
    var passedFirstName = ""
    var passedLastName = ""
    var passedPhoneNumber = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        nameTF.delegate = self
        phoneTF.delegate = self
        emailTF.delegate = self
        nameTF.setBottomBorder()
        phoneTF.setBottomBorder()
        genderTF.setBottomBorder()
        emailTF.setBottomBorder()
        topOrigin = signTopCons.constant
        nameTF.attributedPlaceholder = NSAttributedString(string:"Name",attributes: [NSAttributedString.Key.foregroundColor: UIColor(rgb: 0x898A95)])
        phoneTF.attributedPlaceholder = NSAttributedString(string:"Phone Number",attributes: [NSAttributedString.Key.foregroundColor: UIColor(rgb: 0x898A95)])
        genderTF.attributedPlaceholder = NSAttributedString(string:"Gender",attributes: [NSAttributedString.Key.foregroundColor: UIColor(rgb: 0x898A95)])
        emailTF.attributedPlaceholder = NSAttributedString(string:"Email",attributes: [NSAttributedString.Key.foregroundColor: UIColor(rgb: 0x898A95)])
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: nil, action: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(CreateUserVC.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(CreateUserVC.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        print(passedFirstName)
        print(passedLastName)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        nameTF.text = "\(passedFirstName) \(passedLastName)"
        phoneTF.text = passedPhoneNumber
    }
    
    @IBAction func genderAction(_ sender: UIButton) {
        resignKeyboard()
    }
    
    @IBAction func gustureReco(_ sender: UITapGestureRecognizer) {
        resignKeyboard()
    }
    
    @IBAction func signupAction(_ sender: CustomRoundButton) {
        resignKeyboard()
        let email = emailTF.text!
        if(Ulti.isInternetAvailable()){
            SVProgressHUD.show(withStatus: "Loading")
            createXilnexCustomer(firstName: passedFirstName, lastName: passedLastName, email: email.trimmingCharacters(in: .whitespaces), gender: genderTF.text!, phone: phoneTF.text!)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toGenderSelected"{
            let vc = segue.destination as! GenderPopUpVC
            vc.delegate = self
        }
    }
    
    //MARK: Keyboard appear
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height - 70
            let duration:TimeInterval = (notification.userInfo![UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = notification.userInfo![UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIView.AnimationOptions.curveEaseInOut.rawValue
            let animationCurve:UIView.AnimationOptions = UIView.AnimationOptions(rawValue: animationCurveRaw)
            if(self.presentedViewController == nil){
                self.signTopCons.constant = -keyboardHeight
                UIView.animate(withDuration: duration, delay: TimeInterval(), options: animationCurve, animations: {
                    self.view.layoutIfNeeded()
                }, completion: nil)
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        UIView.animate(withDuration: 2, delay: 0, options: .curveEaseOut, animations: {
            self.signTopCons.constant = self.topOrigin
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    func createXilnexCustomer(firstName: String, lastName: String, email: String, gender: String, phone: String) {
        
        let parameters = ["client": [
            "name":"\(firstName) \(lastName)",
            "email":"\(email)",
            "gender":"\(gender)",
            "firstName":firstName,
            "lastName":lastName,
            "mobile":phone,
            "active":true,
            "allowAllOutlets":true,
            "createdOutlet":"Main Branch",
            "pointFactor":1
            ]] as [String : Any]
        
        let headers = [
            "dkey": "VDEHwLkcwg6pi1P4GUekHpbc4hB8Oh7C",
            "token": "24U3HurDqGbf1oi7nZZTsYkIQDEk2ZYP48x/TWAJ004="
        ]
        
        Alamofire.request(Ulti.urlXilnexCreateNewCustomer, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            
            if(response.error == nil){
                SVProgressHUD.dismiss()
                let alert = UIAlertController(title: "Congratulation", message: "Now you has become a soho club member.", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Login", style: UIAlertAction.Style.default, handler: { (UIAlertAction) in
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "LogInVC")
                    self.navigationController?.pushViewController(vc!, animated: true)
                }))
                alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: { (UIAlertAction) in
                    self.navigationController?.popToRootViewController(animated: true)
                }))
                
                self.present(alert, animated: true, completion: nil)
            }else{
                SVProgressHUD.dismiss()
                print(response.error ?? "")
            }
        }
    }
    
    func resignKeyboard() {
        nameTF.resignFirstResponder()
        phoneTF.resignFirstResponder()
        emailTF.resignFirstResponder()
    }
}

extension CreateUserVC: UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        resignKeyboard()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        resignKeyboard()
        return false
    }
}

extension CreateUserVC: GProtocol{
    func didSelectedGender(gender: String) {
        genderTF.text = gender
    }
    
}
