//
//  SohoBrandListVC.swift
//  soho
//
//  Created by WML on 5/18/18.
//  Copyright © 2018 sohogroup. All rights reserved.
//

import UIKit

class SohoBrandListVC: UIViewController {

    var brandList: [String] = ["CROCS", "FORME", "PENSHOPPE", "SWATCH"]
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

extension SohoBrandListVC: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return brandList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SohoBrandListCell", for: indexPath) as! SohoBrandListCell
        cell.sohoBrandImage.image = UIImage(named: brandList[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.frame.height/5
    }
}
