//
//  roundBtn.swift
//  TestAnimationBounce
//
//  Created by Shilliem Hoeng on 2/12/17.
//  Copyright © 2017 shilliemHoeng. All rights reserved.
//

import UIKit
@IBDesignable
class CustomRoundButton: UIButton {
    @IBInspectable var cornerRadius: CGFloat = 0{
        didSet{
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0{
        didSet{
            self.layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var borderColor: UIColor = UIColor.clear{
        didSet{
            self.layer.borderColor = borderColor.cgColor
        }
    }
}
