//
//  CustomRoundView.swift
//  soho
//
//  Created by WML on 7/11/18.
//  Copyright © 2018 sohogroup. All rights reserved.
//

import UIKit
@IBDesignable
class CustomRoundView: UIView {
    @IBInspectable var cornerRadius: CGFloat = 0{
        didSet{
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0{
        didSet{
            self.layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var borderColor: UIColor = UIColor.clear{
        didSet{
            self.layer.borderColor = borderColor.cgColor
        }
    }
}
