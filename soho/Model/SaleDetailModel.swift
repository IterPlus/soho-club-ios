import Foundation

struct SaleDetailModel: Decodable {
    let itemName: String
    let itemType: String
    let unitPrice: String
    let quantity: String
    let discountPercentage: String
    let discountAmount: String
    let itemCode: String
    let subTotal: String
}
