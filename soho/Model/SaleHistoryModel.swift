
import Foundation

struct SaleHistoryModel {
    let outlet : String
    let saleId : NSNumber
    let amount: NSNumber
    let payAmount: NSNumber
    let saleDate: String
}
