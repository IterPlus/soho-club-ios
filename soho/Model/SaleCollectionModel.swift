//
//  SaleCollectionModel.swift
//  soho
//
//  Created by WML on 5/11/18.
//  Copyright © 2018 sohogroup. All rights reserved.
//

import Foundation

struct SaleCollectionModel: Decodable {
    let method: String
    let paymentDate: String
    let receivedBy: String
}
