import UIKit
import SVProgressHUD
import CoreData
import Alamofire

class SaleHistoriesVC: UIViewController {
    var saleHistoryArray = [SaleHistoryModel]()
    var passedID: String = ""
    var cardsData = [Card]()
    var refreshControl = UIRefreshControl()
    
    @IBOutlet weak var tableview: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        SVProgressHUD.show(withStatus: "Loading")
        loadData()
        navigationController?.navigationBar.backIndicatorImage = UIImage(named: "arrow")
        navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "arrow")
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: nil, action: nil)
        
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(SaleHistoriesVC.loadData), for: UIControl.Event.valueChanged)
        tableview.addSubview(refreshControl)
    }
    
    @objc func loadData(){
        DispatchQueue.main.async {
            let fetchRequest: NSFetchRequest<Card> = Card.fetchRequest()
            let conText = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
            do {
                self.cardsData = try conText.fetch(fetchRequest)
                for data in self.cardsData{
                    self.loadSaleHistory(id: data.id!)
                }
            }catch {
                print("Could not load data from database \(error.localizedDescription)")
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToSaleDetails"{
            let vc = segue.destination as! SaleDetailsVC
            let items = sender as! SaleHistoryModel
            vc.passedSaleId = "\(items.saleId)"
            vc.passedOutlet = "\(items.outlet)"
            vc.passedAmount = "\(items.payAmount)"
        }
    }
    
    func loadSaleHistory(id: String) {
        let header: HTTPHeaders = [
            "token" : Ulti.mSohoToken,
            "dkey" : Ulti.mSohoDkey
        ]
        
        Alamofire.request(Ulti.urlXilnexSaleHistories(id: id), headers: header).responseJSON { (response) in
           
            do {
                guard let data = response.data else {return}
                let sales = try JSONDecoder().decode(SalesHistoryModel.self, from: data)
                for sale in sales.data.purchaseHistories{
                    let outlet = sale.outlet
                    
                    for saleDetail in sale.sales{
                        let saleID = saleDetail.salesId as NSNumber
                        let amount = saleDetail.amount as NSNumber
                        let paidAmount = saleDetail.paidAmount as NSNumber
                        let salesDate = saleDetail.salesDate
                        
                        DispatchQueue.main.async {
                            self.saleHistoryArray.insert(SaleHistoryModel(outlet: outlet, saleId: saleID, amount: amount, payAmount: paidAmount, saleDate: salesDate), at: 0)
                            self.refreshControl.endRefreshing()
                            self.tableview.reloadData()
                            SVProgressHUD.dismiss()
                        }
                    }
                }
            }catch let error as NSError {
                print(error)
                SVProgressHUD.dismiss()
            }
            
        }
    }

    
    
    @objc func detailAction(sender: UIButton){
        performSegue(withIdentifier: "goToSaleDetails", sender: saleHistoryArray[sender.tag])
    }
}

extension SaleHistoriesVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return saleHistoryArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableview.dequeueReusableCell(withIdentifier: "SaleHistoriesCell", for: indexPath) as! SaleHistoriesCell
        let salesHistory = saleHistoryArray[indexPath.row]
        cell.outlet.text = salesHistory.outlet
        cell.saleIDLabel.text = "\(salesHistory.saleId)"
        cell.amountLabel.text = "$\(salesHistory.amount)"
        cell.paidAmountLabel.text = "$\(salesHistory.payAmount)"
        let slitDob = salesHistory.saleDate.components(separatedBy: "T00")
        let resultDob = slitDob[0]
        cell.saleDate.text = resultDob
        cell.detailButton.tag = indexPath.row
        cell.detailButton.addTarget(self, action: #selector(SaleHistoriesVC.detailAction), for: .touchUpInside)
        
        //borderColor,borderWidth, cornerRadius
        cell.containerView.layer.borderColor = UIColor.white.cgColor
        cell.containerView.layer.borderWidth = 1
        cell.containerView.layer.cornerRadius = 8
        cell.containerView.clipsToBounds = true
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "goToSaleDetails", sender: saleHistoryArray[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.view.frame.height/5
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
}

struct SalesHistoryModel: Decodable {
    var data: SalesData
}

struct SalesData: Decodable {
    var purchaseHistories: [SalesPurchaseHistories]
}

struct SalesPurchaseHistories: Decodable {
    var outlet: String = ""
    var sales: [SalesDetail]
}

struct SalesDetail: Decodable {
    var salesId: Int = 0
    var amount: Double = 0.0
    var paidAmount: Double = 0.0
    var balanceAmount: Int = 0
    var salesDate: String = ""
}
