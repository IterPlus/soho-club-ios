
import UIKit
import SVProgressHUD
class PromotionVC: UIViewController {
    
    @IBOutlet weak var webview: UIWebView!
    var passedLink: String = ""
    private var lastContentOffset: CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if (Ulti.isInternetAvailable()){
            SVProgressHUD.show(withStatus: "Loading")
            webview.delegate = self
            webview.loadRequest(URLRequest(url: URL(string: passedLink)!))
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        SVProgressHUD.dismiss()
    }
}

extension PromotionVC: UIWebViewDelegate, UIScrollViewDelegate{
    func webViewDidFinishLoad(_ webView: UIWebView) {
        SVProgressHUD.dismiss()
    }

    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        SVProgressHUD.dismiss()
    }
}
