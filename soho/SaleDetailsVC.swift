//
//  saleDetailsVC.swift
//  soho
//
//  Created by WML on 5/11/18.
//  Copyright © 2018 sohogroup. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD

class SaleDetailsVC: UIViewController {

    @IBOutlet weak var outletLabel: UILabel!
    @IBOutlet weak var invoiceNoLabel: UILabel!
    @IBOutlet weak var chashirerLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var outletImage: UIImageView!
    @IBOutlet weak var discountLabel: UILabel!
    @IBOutlet weak var grandTotalLabel: UILabel!
    
    var passedSaleId: String = ""
    var passedOutlet: String = ""
    var passedAmount: String = ""
    var totalDiscount:Int = 0
    var returnPolicyViewHOrigin: CGFloat = 0
    var isPolicyViewShow: Bool = true
    var itemCode: String = ""
    var itemName: String = ""
    var quantity: NSNumber = 0
    var discountPercentage: NSNumber = 0
    var itemType: String = ""
    var discountAmount: NSNumber = 0
    var unitPrice: NSNumber = 0
    var salePerson: String = ""
    var categoty: String = ""
    var method: String = ""
    var paymentDate: String = ""
    var receivedBy: String = ""
    var subTotal: NSNumber = 0
    var brand: String = ""
    var details = [SaleDetailModel]()
    var collections = [SaleCollectionModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SVProgressHUD.show(withStatus: "Loading")
        var outlet = passedOutlet.components(separatedBy: " ")
        if(outlet[0] == "HAUTE"){
            outlet[0] = outlet[0] + " RACK"
        }
        outletLabel.text = passedOutlet
        invoiceNoLabel.text = passedSaleId
        grandTotalLabel.text = "$\(passedAmount)"
        outletImage.image = UIImage(named: outlet[0])
        loadSaleDetail()
    }
    
    func loadSaleDetail() {
        let url = Ulti.urlXilnexSalesInVoice + passedSaleId
        let headers: HTTPHeaders = [
            "token": Ulti.mSohoToken,
            "dkey": Ulti.mSohoDkey
        ]
        
        Alamofire.request(url, method: .get, headers: headers).responseJSON { (res) in
            do{
                guard let data = res.data else {return}
                let saleDetails = try JSONDecoder().decode(InvoiceItemDetailModel.self, from: data)
                for saleDetail in saleDetails.data.sale.items{
                    self.subTotal = saleDetail.subtotal as NSNumber
                    self.itemCode = saleDetail.itemCode
                    self.itemName = saleDetail.itemName
                    self.quantity = saleDetail.quantity as NSNumber
                    self.discountPercentage = saleDetail.discountPercentage as NSNumber
                    self.itemType = saleDetail.itemType
                    self.discountAmount = saleDetail.discountAmount as NSNumber
                    self.unitPrice = saleDetail.unitPrice as NSNumber
                    let format = NumberFormatter()
                    format.numberStyle = .decimal
                    format.minimumFractionDigits = 2
                    let formatPrice = format.string(from: self.unitPrice)
                    self.salePerson = saleDetail.salesPerson
                    self.categoty = saleDetail.category
                    self.totalDiscount = self.totalDiscount + Int(truncating: self.discountAmount)
                    self.discountLabel.text = "$\(self.totalDiscount)"
                    self.details.append(SaleDetailModel(itemName: self.itemName, itemType: self.itemType, unitPrice: formatPrice!, quantity: "\(self.quantity)", discountPercentage: "\(self.discountPercentage)", discountAmount: "\(self.discountAmount)", itemCode: self.itemCode, subTotal: "\(self.subTotal)"))
                    self.tableview.reloadData()
                }

                for saleCollection in saleDetails.data.sale.collections{
                    let paymentDate = saleCollection.paymentDate
                    let slitDate = paymentDate.components(separatedBy: "T")
                    let resultDate = slitDate[0]
                    self.dateLabel.text = resultDate
                    self.chashirerLabel.text = saleCollection.receivedBy
                }
                
                SVProgressHUD.dismiss()
            }catch let error as NSError {
                print(error)
                SVProgressHUD.dismiss()
            }
        }
    }
}

extension SaleDetailsVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return details.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SaleDetailsCell", for: indexPath) as! SaleDetailsCell
        let detail = details[indexPath.row]
        cell.descriptionLabel.text = detail.itemName
        cell.qtyLabel.text = detail.quantity
        cell.priceLabel.text = "$\(detail.unitPrice)"
        cell.disLabel.text = "$\(detail.discountAmount)"
        cell.totalLabel.text = "$\(detail.subTotal)"
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 25
    }
}

struct InvoiceItemDetailModel: Decodable {
    var data: InvoiceDataModel
}

struct InvoiceDataModel: Decodable {
    var sale: InvoiceSaleModel
}

struct InvoiceSaleModel: Decodable {
    var items: [InvoiceItemsnModel]
    var collections: [InvoiceCollectionModel]
}

struct InvoiceCollectionModel: Decodable {
    var paymentDate: String = ""
    var receivedBy: String = ""
}

struct InvoiceItemsnModel: Decodable {
    var subtotal: Double = 0.0
    var itemCode: String = ""
    var itemName: String = ""
    var quantity: Int = 0
    var discountPercentage: Int = 0
    var itemType: String = ""
    var discountAmount: Double = 0.0
    var unitPrice: Double = 0.0
    var salesPerson: String = ""
    var category: String = ""
}
