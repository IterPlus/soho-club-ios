
import UIKit
import SVProgressHUD
import Alamofire

class LogInVC: UIViewController {
    
    @IBOutlet weak var connectionView: UIView!
    @IBOutlet weak var phoneNumberTF: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        phoneNumberTF.layer.borderColor = UIColor.red.cgColor
        phoneNumberTF.layer.borderWidth = 1
        phoneNumberTF.layer.cornerRadius = phoneNumberTF.frame.height/2
        phoneNumberTF.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(LogInVC.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(LogInVC.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        hideLoading()
        Ulti.checkConnection(connectionView: connectionView)
    }
    
    @objc
    func keyboardWillShow(notification: NSNotification) {
        if self.view.frame.origin.y == 0{
            self.view.frame.origin.y -= 50
        }
    }
    
    @objc
    func keyboardWillHide(notification: NSNotification) {
        self.view.frame.origin.y = 0
    }
    
    @IBAction func nextAction(_ sender: UIBarButtonItem) {
        if (phoneNumberTF.text != "") {
            if (Ulti.isInternetAvailable()){
                showLoading()
                DispatchQueue.main.async {
                    self.cardAdded(mobile: self.phoneNumberTF.text!, token: Ulti.mSohoToken, dkey: Ulti.mSohoDkey)
                }
            }else{
                connectionView.isHidden = false
            }
        }else{
            self.present(Ulti.showMessage(title: Ulti.messageTitle, message: Ulti.loginNullSMS, actionTitle: "cancel"), animated: true, completion: {})
        }
        resignKeyboard()
    }
    
    @IBAction func viewTapGesture(_ sender: UITapGestureRecognizer) {
        resignKeyboard()
    }
    
    func cardAdded(mobile: String, token: String, dkey: String){
        resignKeyboard()
        showLoading()
        
        let headers: HTTPHeaders = [
            "token": token,
            "dkey": dkey
        ]
        let url = Ulti.urlXilnexSearched + mobile
        Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            do {
                
                guard let data = response.data else {return}
                let users = try JSONDecoder().decode(LoginModel.self, from: data)
                for user in users.data.clients {
                    
                    DispatchQueue.main.async(execute: {
                        let name = user.name
                        let point = "\(user.pointValue)"
                        let email = user.email
                        let serial = user.alternateLookup
                        let dob = user.dob
                        let slitDob = dob.components(separatedBy: "T00")
                        let resultDob = slitDob[0]
                        let id = "\(user.id)"
                        
                        Ulti.saveMemberCard(serial: serial, name: name, id: id, dob: resultDob, phone: self.phoneNumberTF.text!, point: point, email: email, outlet: Ulti.mSoho, logo: Ulti.mSohoLogoName, token: Ulti.mSohoToken, dkey: Ulti.mSohoDkey)
                        
                        if Ulti.fetchSignUp() == "" {
                            Ulti.saveIsCheckSignUp(check: Ulti.SIGN_IN)
                        }
                    })
                }
                
            } catch let error as NSError {
                print(error)
            }
        }
    }
    
    func LoginToDo(){
        Ulti.subStoryBoard()
    }
    
    func hideLoading() {
        connectionView.isHidden = false
        SVProgressHUD.dismiss()
    }
    
    func showLoading() {
        SVProgressHUD.show(withStatus: "Loading")
    }
    
    func resignKeyboard(){
        phoneNumberTF.resignFirstResponder()
    }
}

extension LogInVC: UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        resignKeyboard()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
}

struct LoginModel: Decodable {
    var data: LoginData
}

struct LoginData: Decodable {
    var clients: [LoginClients]
}

struct LoginClients: Decodable {
    var name: String = ""
    var pointValue: Int = 0
    var email: String = ""
    var alternateLookup: String = ""
    var dob: String = ""
    var id: Int = 0
    var mobile: String = ""
}
