//
//  PromotionMenuVC.swift
//  soho
//
//  Created by WML on 9/21/18.
//  Copyright © 2018 sohogroup. All rights reserved.
//

import UIKit
import SDWebImage
import SVProgressHUD

class PromotionMenuVC: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    let menu = [Ulti.PROMO_SWATCH, Ulti.PROMO_BIRKENSTOCK, Ulti.PROMO_OXGN, Ulti.PROMO_METALLURGY, Ulti.PROMO_FORME, Ulti.PROMO_INORI, Ulti.PROMO_BONCHON, Ulti.PROMO_CROCS, Ulti.PROMO_HAUTERACK, Ulti.PROMO_HAVAIANAS, Ulti.PROMO_PENSHOPPE, Ulti.PROMO_ROKKU, Ulti.PROMO_SANFRANCISCO, Ulti.PROMO_SPRAYWAY]
    
    lazy var gridLayout: GridLayout = {
        var gridLayout = GridLayout(numberOfColumns: 2)
        return gridLayout
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.collectionViewLayout = gridLayout
    }
    
    override func viewWillAppear(_ animated: Bool) {
        SVProgressHUD.dismiss()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "goToPromotion"){
            let vc = segue.destination as! PromotionVC
            vc.passedLink = sender as! String
        }
    }
}

extension PromotionMenuVC: UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return menu.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PromotionMenuCell", for: indexPath) as! PromotionMenuCell
        cell.promotionImage.image = UIImage(named: menu[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        var url: String = ""
        switch menu[indexPath.row] {
        case Ulti.PROMO_BIRKENSTOCK:
            url = Ulti.mPromoBirkenstock
        case Ulti.PROMO_SWATCH:
            url = Ulti.mPromoSwatch
        case Ulti.PROMO_OXGN:
            url = Ulti.mPromoOxygen
        case Ulti.PROMO_METALLURGY:
            url = Ulti.mPromoMetallurgy
        case Ulti.PROMO_FORME:
            url = Ulti.mPromoForme
        case Ulti.PROMO_INORI:
            url = Ulti.mPromoInori
        case Ulti.PROMO_BONCHON:
            url = Ulti.mPromoBonchon
        case Ulti.PROMO_CROCS:
            url = Ulti.mPromoCrocs
        case Ulti.PROMO_HAUTERACK:
            url = Ulti.mPromoHauteRack
        case Ulti.PROMO_HAVAIANAS:
            url = Ulti.mPromoHavaianas
        case Ulti.PROMO_PENSHOPPE:
            url = Ulti.mPromoPenshoppe
        case Ulti.PROMO_ROKKU:
            url = Ulti.mPromoRokku
        case Ulti.PROMO_SANFRANCISCO:
            url = Ulti.mPromoSanFrancisco
        case Ulti.PROMO_SPRAYWAY:
            url = Ulti.mPromoSprayWay
            
        default:
            break
        }
        performSegue(withIdentifier: "goToPromotion", sender: url)
    }
    
}
