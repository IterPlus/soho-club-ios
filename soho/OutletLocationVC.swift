//
//  OutletLocationVC.swift
import UIKit
import MapKit
import CoreLocation
import Contacts
import SVProgressHUD

class OutletLocationVC: UIViewController {

    @IBOutlet weak var collectionview: UICollectionView!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var outletMapDetailImage: UIImageView!
    @IBOutlet weak var outleDetailtLabel: UILabel!
    @IBOutlet weak var addressDetailLabel: UILabel!
    private var currentCoordinate: CLLocationCoordinate2D?
    @IBOutlet weak var containerDetailViewBottomConstraint: NSLayoutConstraint!
    
    let regionRadius: CLLocationDistance = 2000
    let locationManager = CLLocationManager()
    var currentLocation: CLLocationCoordinate2D?
    let outlets: [OutletAnnotationModel] = []
    let brandArray: [String] = [Ulti.mCrocs, Ulti.mForME, Ulti.mPenshoppe, Ulti.mSwatch, Ulti.mHavaianas, Ulti.mBirkenStock, Ulti.mHauteRack, Ulti.mHavaianas, Ulti.mINoRiJewels, Ulti.mMetallurgy, Ulti.mOxygen, Ulti.mBonchon, Ulti.mSprayWay, Ulti.mSanFrancisco, Ulti.mRokku, "ALL"]
    var brandSelected: String = "ALL"
    var allOutlets: [OutletAnnotationModel] = []
    var coords: CLLocationCoordinate2D?
    var isDetailClosed = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureLocationServices()
        mapView.delegate = self
        allOutlets = Ulti.outlets()
        mapAnnotation(annotations: allOutlets)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        SVProgressHUD.dismiss()
    }
    
    @IBAction func closeDetailViewAction(_ sender: UIButton) {
        if(isDetailClosed){
            UIView.animate(withDuration: 2, delay: 0, options: [], animations: {
                self.containerDetailViewBottomConstraint.constant = 0
                self.isDetailClosed = false
            }, completion: nil)
        }else{
            UIView.animate(withDuration: 2, delay: 0, options: [], animations: {
                self.containerDetailViewBottomConstraint.constant = 130
                self.isDetailClosed = true
            }, completion: nil)
        }
    }
    
    func mapAnnotation(annotations: [OutletAnnotationModel]){
        mapView.removeAnnotations(mapView.annotations)
        print("mapAnnotation")
        if(brandSelected != "ALL"){
            for annotation in annotations{
                let title = annotation.title
                let titleArray = title?.components(separatedBy: " ")
                if(titleArray![0] == brandSelected){
                    mapView.addAnnotation(annotation)
                    print(annotation)
                }
            }
        }else{
            for annotation in annotations{
                
                print("annotation\(annotation)")
                mapView.addAnnotation(annotation)
            }
        }
        
    }
    
    private func zoomToLatestLocation(with coordinate: CLLocationCoordinate2D) {
        let zoomRegion = MKCoordinateRegion.init(center: coordinate, latitudinalMeters: 10000, longitudinalMeters: 10000)
        mapView.setRegion(zoomRegion, animated: true)
    }
    
    private func configureLocationServices() {
        locationManager.delegate = self
        let status = CLLocationManager.authorizationStatus()
        if status == .notDetermined {
            locationManager.requestAlwaysAuthorization()
        } else if status == .authorizedAlways || status == .authorizedWhenInUse {
            beginLocationUpdates(locationManager: locationManager)
        }
    }
    
    private func beginLocationUpdates(locationManager: CLLocationManager) {
        mapView.showsUserLocation = true
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
    }
    
    func setAnnotationView(annotation: MKAnnotation, mapView: MKMapView) -> MKAnnotationView? {
        if(annotation is MKUserLocation){
            return nil
        }else{
            let reuseIdentifier = "pin"
            var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseIdentifier)
            
            if annotationView == nil {
                annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseIdentifier)
                annotationView?.canShowCallout = true
            } else {
                annotationView?.annotation = annotation
            }
            
            if(brandSelected == "ALL"){
                let title = annotation.title
                let titleArray = title??.components(separatedBy: " ")
                updateAnnotationView(brand: titleArray![0])
                annotationView?.image = UIImage(named: brandSelected)
                brandSelected = "ALL"
            }else{
                updateAnnotationView(brand: brandSelected)
                annotationView?.image = UIImage(named: brandSelected)
            }
            
            return annotationView
        }
    }
    
    func updateAnnotationView(brand: String) {
        switch brand {
        case Ulti.mCrocs:
            brandSelected = Ulti.mCrocsPin
        case Ulti.mPenshoppe:
            brandSelected = Ulti.mPenshoppePin
        case Ulti.mSwatch:
            brandSelected = Ulti.mSwatchPin
        case Ulti.mForME:
            brandSelected = Ulti.mForMePin
        case Ulti.mHavaianas:
            brandSelected = Ulti.mHavaianasPin
        case Ulti.mMetallurgy:
            brandSelected = Ulti.mMetallurgyPin
        case Ulti.mOxygen:
            brandSelected = Ulti.mOxygenPin
        case Ulti.mBonchon:
            brandSelected = Ulti.mBonchonPin
        case Ulti.mINoRiJewels:
            brandSelected = Ulti.mInoriPin
        case Ulti.mSprayWay:
            brandSelected = Ulti.mSprayWayPin
        case Ulti.mBirkenStock:
            brandSelected = Ulti.mBirkenStockPin
        case Ulti.mHauteRack:
            brandSelected = Ulti.mHauteRackPin
        case Ulti.mSanFrancisco:
            brandSelected = Ulti.mSanFranciscoPin
        case Ulti.mRokku:
            brandSelected = Ulti.mRokkuPin
        default:
            break
        }
    }
}

extension OutletLocationVC: MKMapViewDelegate, CLLocationManagerDelegate{
    //MARK: - Custom Annotation
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        return setAnnotationView(annotation: annotation, mapView: mapView)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let latestLocation = locations.first else { return }
        if currentCoordinate == nil {
            zoomToLatestLocation(with: latestLocation.coordinate)
        }
        currentCoordinate = latestLocation.coordinate
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        print("The status changed")
        if status == .authorizedAlways || status == .authorizedWhenInUse {
            beginLocationUpdates(locationManager: manager)
        }
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        let outletDetailName = (view.annotation?.title)!
        let outletDetailAddress = (view.annotation?.subtitle)!
        outleDetailtLabel.text = outletDetailName
        addressDetailLabel.text = outletDetailAddress
        outletMapDetailImage.image = UIImage(named: outletDetailName!)
        
        UIView.animate(withDuration: 2, delay: 0, options: [], animations: {
            self.containerDetailViewBottomConstraint.constant = 0
            self.isDetailClosed = false
        }, completion: nil)
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        
    }
}

extension OutletLocationVC: UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return brandArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OutletLocationCell", for: indexPath) as! OutletLocationCell
        cell.brandLabel.text = brandArray[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        brandSelected = brandArray[indexPath.row]
        mapAnnotation(annotations: allOutlets)
    }
}
