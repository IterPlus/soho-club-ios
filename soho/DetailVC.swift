//
//  DetailVC.swift
//  soho
//
//  Created by WML on 5/7/18.
//  Copyright © 2018 sohogroup. All rights reserved.
//

import UIKit

class DetailVC: UIViewController {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var qrCodeLabel: UILabel!
    @IBOutlet weak var pointLabel: UILabel!
    @IBOutlet weak var dobLabel: UILabel!
    @IBOutlet weak var qrCodeImage: UIImageView!
    
    var passedInfo = [Card]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for items in passedInfo{
            nameLabel.text = items.name
            phoneLabel.text = items.phone
            qrCodeLabel.text = items.serial
            pointLabel.text = items.point
            dobLabel.text = items.dob
            qrCodeImage.image = Ulti.qrCodeGenarator(string: items.serial!)
        }
    }
    
    
}
