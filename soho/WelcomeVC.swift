import UIKit

class WelcomeVC: UIViewController {
    
    @IBOutlet weak var loginBtn: CustomRoundButton!
    @IBOutlet weak var orLabel: UILabel!
    @IBOutlet weak var registerBtn: CustomRoundButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.navigationController?.navigationBar.backIndicatorImage = UIImage(named: "arrow")
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "arrow")
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: nil, action: nil)
        animate()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        super.viewWillDisappear(animated)
    }

    func animate() {
        
        UIView.animate(withDuration: 0.2) {
            self.loginBtn.isHidden = false
            self.orLabel.isHidden = false
            self.registerBtn.isHidden = false
        }
    }
}
