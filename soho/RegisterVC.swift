//
//  RegisterVC.swift
//  soho
//
//  Created by WML on 8/15/18.
//  Copyright © 2018 sohogroup. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD

class RegisterVC: UIViewController {

    @IBOutlet weak var logoTopConstaint: NSLayoutConstraint!
    @IBOutlet weak var masterCardNumberTF: UITextField!
    @IBOutlet weak var phoneNumberTF: UITextField!
    var first_name = ""
    var last_name = ""
    var logoTopConsOrigin: CGFloat = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        masterCardNumberTF.delegate = self
        phoneNumberTF.delegate = self
        logoTopConsOrigin = logoTopConstaint.constant
        masterCardNumberTF.attributedPlaceholder = NSAttributedString(string:"Master Card Number",attributes: [NSAttributedString.Key.foregroundColor: UIColor(rgb: 0x898A95)])
        phoneNumberTF.attributedPlaceholder = NSAttributedString(string:"Phone Number",attributes: [NSAttributedString.Key.foregroundColor: UIColor(rgb: 0x898A95)])
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: nil, action: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(RegisterVC.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(RegisterVC.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        masterCardNumberTF.setBottomBorder()
        phoneNumberTF.setBottomBorder()
//        masterCardNumberTF.text = "5363657327850006"
//        phoneNumberTF.text = "0966711659"
        
//        masterCardNumberTF.text = "5363655004733573"
//        phoneNumberTF.text = "092916640"
    }

    @IBAction func NextAction(_ sender: CustomRoundButton) {
        resignKeyboard()
        if(masterCardNumberTF.text != "" && phoneNumberTF.text != ""){
            SVProgressHUD.show(withStatus: "Loading")
            requestToken(cardNumber: masterCardNumberTF.text!, phone: phoneNumberTF.text!)
        }
    }
    
    @IBAction func gustureRecog(_ sender: UITapGestureRecognizer) {
        resignKeyboard()
    }
    
    func resignKeyboard() {
        masterCardNumberTF.resignFirstResponder()
        phoneNumberTF.resignFirstResponder()
    }
    
    //MARK: Keyboard appear
    @objc func keyboardWillShow(notification: NSNotification) {
        let duration:TimeInterval = (notification.userInfo![UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
        let animationCurveRawNSN = notification.userInfo![UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber
        let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIView.AnimationOptions.curveEaseInOut.rawValue
        let animationCurve:UIView.AnimationOptions = UIView.AnimationOptions(rawValue: animationCurveRaw)
        
        if(self.presentedViewController == nil){
            self.logoTopConstaint.constant = logoTopConsOrigin - 50
            UIView.animate(withDuration: duration, delay: TimeInterval(), options: animationCurve, animations: {
                self.view.layoutIfNeeded()
            }, completion: nil)
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        UIView.animate(withDuration: 2, delay: 0, options: .curveEaseOut, animations: {
            self.logoTopConstaint.constant = self.logoTopConsOrigin
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    func requestToken(cardNumber: String, phone: String){
        print("requestToken")
        Alamofire.request(Ulti.urlIterPlusRequestToken, method: .get).responseJSON { (res) in
            do{
                guard let data = res.data else { return }
                let token = try JSONDecoder().decode(RequestTokenModel.self, from: data)
                print(token)
                self.verifyMasterCard(token: token.access_token, cardNumber: cardNumber, phone: phone)
            } catch let error as NSError {
                SVProgressHUD.dismiss()
                print(error)
            }
        }
    }
    
    func verifyMasterCard(token: String, cardNumber: String, phone: String) {
        
        let params = [
            "token" : token,
            "card" : cardNumber,
            "phone" : phone
        ]
        
        Alamofire.request(Ulti.urlIterPlusVertifyMasterCard, method: .post, parameters: params).responseJSON { (res) in
            guard let data = res.data else { return }
            do{
                
                print("loginBelove json = \(data)")
                let user = try JSONDecoder().decode(UserInfo.self, from: data)
                print(user.first_name)
                self.first_name = user.first_name
                self.last_name = user.last_name
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                    self.performSegue(withIdentifier: "toCreateUser", sender: nil)
                }
                
            }catch let error as NSError {
                print("loadHomePage error = \(error)")
                SVProgressHUD.dismiss()
                let alertController = UIAlertController(title: "Alert", message:
                    "The card isnot register via WING!", preferredStyle: UIAlertController.Style.alert)
                alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default,handler: nil))
                
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toCreateUser"{
            let vc = segue.destination as! CreateUserVC
            vc.passedLastName = last_name
            vc.passedFirstName = first_name
            vc.passedPhoneNumber = phoneNumberTF.text!
        }
    }
}

extension RegisterVC: UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        resignKeyboard()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        resignKeyboard()
        return false
    }
}

extension UITextField {
    func setBottomBorder() {
        print("setBottomBorder")
        self.borderStyle = .none
        self.layer.backgroundColor = UIColor(rgb: 0x242424).cgColor
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.white.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 0.0
    }
}

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
}


struct UserInfo: Decodable {
    var first_name: String = ""
    var last_name: String = ""
}


struct RequestTokenModel: Decodable {
    var access_token: String = ""
}
