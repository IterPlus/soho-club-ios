
import UIKit
import CoreData
import SVProgressHUD
import Alamofire

class MainVC: UIViewController {
    @IBOutlet weak var pointLabel: UILabel!
    
    var cardsData = [Card]()
    var cardNameLite: String = ""
    var pointLite: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.hideLoading()
    }
   
    @IBAction func signoutAction(_ sender: UIBarButtonItem) {
        if(Ulti.isInternetAvailable()){
            let alert = UIAlertController(title: "Logout", message: "Do you want to logout?", preferredStyle: UIAlertController.Style.actionSheet)
            alert.addAction(UIAlertAction(title: "Logout", style: UIAlertAction.Style.default, handler:{ (UIAlertAction)in
                
                Ulti.deleteIsCheckSignUp()
                Ulti.deleteAllCard()
                Ulti.deleteIsCheckSync()
                
                let vc = UIStoryboard(name: "Main", bundle:nil).instantiateViewController(withIdentifier: "MainController") as UIViewController
                let appDelegate = (UIApplication.shared.delegate as! AppDelegate)
                appDelegate.window?.rootViewController = vc
            }))
            
            alert.addAction(UIAlertAction(title: "cancel", style: UIAlertAction.Style.cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }else{
            self.present(Ulti.showMessage(title: "Alert", message: "No Internet Connection", actionTitle: "OK"), animated: true, completion: nil)
        }
    }
    
    @IBAction func outletAction(_ sender: UIButton) {
        self.tabBarController?.tabBar.isHidden = true
    }
    
    func loadData(){
        DispatchQueue.main.async {
            let fetchRequest: NSFetchRequest<Card> = Card.fetchRequest()
            let conText = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
            do {
                self.cardsData = try conText.fetch(fetchRequest)
                for data in self.cardsData{
                    self.pointLabel.text = data.point!
                }
            }catch {
                print("Could not load data from database \(error.localizedDescription)")
            }
        }
    }
    
    func cardLoadData(mobile: String, token: String, dkey: String){
        let url = Ulti.urlXilnexSearched + mobile
        let headers: HTTPHeaders = [
            "token": token,
            "dkey": dkey
        ]
        
        Alamofire.request(url, method: .get, headers: headers).response { (response) in
            do {
                
                guard let data = response.data else {return}
                let users = try JSONDecoder().decode(LoginModel.self, from: data)
                for user in users.data.clients {
                    
                    DispatchQueue.main.async(execute: {
                        let phone = user.mobile
                        let name = user.name
                        let point = "\(user.pointValue)"
                        let serial = user.alternateLookup
                        
                        if(self.pointLite != point || self.cardNameLite != name){
                            Ulti.updateData(cardNo: serial, cardName: name, mobile: phone, point: point)
                            self.loadData()
                        }else{
                            print("Same Same")
                        }
                    })
                }
                
            } catch let error as NSError {
                print(error)
            }
        }
    }
    
    func hideLoading() {
        SVProgressHUD.dismiss()
    }
    
    func showLoading() {
        SVProgressHUD.show(withStatus: "Loading")
    }
}
