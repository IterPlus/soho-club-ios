import UIKit
import CoreData
import AVFoundation
import SystemConfiguration
import MapKit

class Ulti{
    
    //CROCS
    static let mCrocs = "CROCS"
    static let mCrocsAeonMallTitle = "CROCS Store AEON Mall Phnom Penh"
    static let mCrocsAeonMallAddress = "CROCS Aeon Mall, 1st Floor, #132, St. Sothearos, Tonle Bassac, Chamkarmon, Phnom Penh"
    static let mCrocsAeonMallCoordinate = CLLocationCoordinate2D(latitude: 11.5997746, longitude: 104.8504244)
    
    static let mCrocsTKAvenueTitle = "CROCS Store TK Avenue"
    static let mCrocsTKAvenueAddress = "St. 315 (Corner St. 516), Toul Kork, Phnom Penh"
    static let mCrocsTKAvenueCoordinate = CLLocationCoordinate2D(latitude: 11.583628, longitude: 104.899152)
    
    static let mCrocsExchangeSquareTitle = "CROCS Store Exchange Square"
    static let mCrocsExchangeSquareAddress = "Exchange Square Mall, 1st Floor #28, St. 106 (Corner St. 51), Duan Penh, Phnom Penh"
    static let mCrocsExchangeSquareCoordinate = CLLocationCoordinate2D(latitude: 11.573696, longitude: 104.921359)
    
    static let mCrocsSihanoukBlvdTitle = "CROCS Store Sihanouk Blvd"
    static let mCrocsSihanoukBlvdAddress = "No. 44 Sihanouk Blvd. Tonle Bassac Phnom Penh, Cambodia"
    static let mCrocsSihanoukBlvdCoordinate = CLLocationCoordinate2D(latitude: 11.554036, longitude: 104.932678)
    
    static let mCrocsAEONMallSenSokTitle = "CROCS Store AEON Mall Sen Sok City"
    static let mCrocsAEONMallSenSokAddress = "AEON Mall Sen Sok City, Ground Floor, St. 1003 Village Bayab, Sangkat Phnom Penh Thmey, Khan Sen Sok, Phnom Penh, Cambodia"
    static let mCrocsAEONMallSenSokCoordinate = CLLocationCoordinate2D(latitude: 11.5996873, longitude: 104.8832553)
    
    static let mCrocsSoryaMallTitle = "CROCS Store Sorya Mall"
    static let mCrocsSoryaMallAddress = "2nd Floor, Street 63, Phsar Thmey, Daun Penh, Phnom Penh"
    static let mCrocsSoryaMallCoordinate = CLLocationCoordinate2D(latitude: 11.567816, longitude: 104.920849)
    
    //FORME
    static let mForME = "FORME"
    static let mForMeAeonMallTitle = "FORME AEON Mall Phnom Penh"
    static let mForMeAeonMallAddress = "No. 132 Sothearos Blvd. Unit #0013 (Located on the 1st Floor), Phnom Penh"
    static let mForMeAeonMallCoordinate = CLLocationCoordinate2D(latitude: 11.5997746, longitude: 104.8504244)
    
    //SWATCH
    static let mSwatch = "SWATCH"
    static let mSwatchAeonMallTitle = "SWATCH AEON Mall Phnom Penh"
    static let mSwatchAeonMallAddress = "AEON Mall Ground Floor Fashion. #132, Street Samdach Sothearos, Sangkat Tonle Bassac, Khan Chamkarmon, Phnom Penh, Cambodia"
    static let mSwatchAeonMallCoordinate = CLLocationCoordinate2D(latitude: 11.5997746, longitude: 104.8504244)
    
    static let mSwatchExchangeSquareTitle = "SWATCH Exchange Square Mall"
    static let mSwatchExchangeSquareAddress = "Exchange Square Mall Ground Floor, St. 106 corner St. 61, Sangkat Wat Phnom, Khan Daun Penh, Phnom Penh, Cambodia"
    static let mSwatchExchangeSquareCoordinate = CLLocationCoordinate2D(latitude: 11.573840, longitude: 104.921190)
    
    static let mSwatchAEONMallSenSokTitle = "SWATCH AEON Mall Sen Sok City"
    static let mSwatchAEONMallSenSokAddress = "AEON Mall Sen Sok City, Ground Floor, St. 1003 Village Bayab, Sangkat Phnom Penh Thmey, Khan Sen Sok, Phnom Penh, Cambodia."
    static let mSwatchAEONMallSenSokCoordinate = CLLocationCoordinate2D(latitude: 11.5996873, longitude: 104.8832553)
    
    //PENSHOPPE
    static let mPenshoppe = "PENSHOPPE"
    static let mPenshoppeAeonMallTitle = "PENSHOPPE AEON Mall Phnom Penh"
    static let mPenshoppeAeonMallAddress = "132 Street Samdach Sothearos, Sangkat Tonle Bassac, Khan Chamkarmon, Phnom Penh, Cambodia (Located on the Ground Floor)"
    static let mPenshoppeAeonMallCoordinate = CLLocationCoordinate2D(latitude: 11.5997746, longitude: 104.8504244)

    static let mPenshoppeTKTitle = "PENSHOPPE TK Avenue"
    static let mPenshoppeTKAddress = "No. 80, St. 315, Corner of St. 516 Toul Kork, Phnom Penh, Cambodia, 12151"
    static let mPenshoppeTKCoordinate = CLLocationCoordinate2D(latitude: 11.583974, longitude: 104.899055)
    
    static let mPenshoppeLuckyMallTitle = "PENSHOPPE Lucky Mall"
    static let mPenshoppeLuckyMallAddress = "#E1-02, St. Sivutha ,Mondul II Village,Svay Dangkum | Siem Reap, Cambodia"
    static let mPenshoppeLuckyMallCoordinate = CLLocationCoordinate2D(latitude: 13.362713, longitude: 103.855515)
    
    static let mPenshoppeAeonMallSenSokTitle = "PENSHOPPE AEON Mall Sen Sok City"
    static let mPenshoppeAeonMallSenSokAddress = "AEON Mall 2 St. 1003, village Bayab, Commune, Phnom Penh"
    static let mPenshoppeAeonMallSenSokCoordinate = CLLocationCoordinate2D(latitude: 11.5996873, longitude: 104.8832553)
    
    //BIRKENSTOCK
    static let mBirkenStock = "BIRKENSTOCK"
    static let mBirkenstockAEONMallSenSokTitle = "BIRKENSTOCK AEON Mall Sen Sok City"
    static let mBirkenstockAEONMallSenSokAddress = "AEON Mall Sen Sok City, Ground Floor, St. 1003 Village Bayab, Sangkat Phnom Penh Thmey, Khan Sen Sok, Phnom Penh, Cambodia"
    static let mBirkenstockAEONMallSenSokCoordinate = CLLocationCoordinate2D(latitude: 11.5996873, longitude: 104.8832553)
    
    //HauteRackSquare
    static let mHauteRack = "HAUTERACK"
    static let mHauteRackAeonSenSokTitle = "HAUTERACK AEON Mall Sen Sok City"
    static let mHauteRackAeonSenSokAddress = "AEON Mall Sen Sok City, Ground Floor, St. 1003 Village Bayab, Sangkat Phnom Penh Thmey, Khan Sen Sok, Phnom Penh, Cambodia"
    static let mHauteRackAeonSenSokCoordinate = CLLocationCoordinate2D(latitude: 11.5996873, longitude: 104.8832553)
    
    //Havaianas
    static let mHavaianas = "HAVAIANAS"
    static let mHavaianasAEONMallTitle = "HAVAIANAS AEON Mall"
    static let mHavaianasAEONMallAdddress = "No. 132 Sothearos Blvd. Unit #0013 (Located on the Ground Floor), Phnom Penh"
    static let mHavaianasAEONMallCoordinate = CLLocationCoordinate2D(latitude: 11.547784, longitude: 104.933320)
    
    static let mHavaianasPP63Title = "HAVAIANA PP St. 63"
    static let mHavaianasPP63Adddress = "No. 203 Street 63, Sangkat Beng Keng Kong I, Khan Chamkar Mon, Phnom Penh"
    static let mHavaianasPP63Coordinate = CLLocationCoordinate2D(latitude: 11.551682, longitude: 104.923650)
    
    static let mHavaianasBKKTitle = "HAVAIANAS H Shop BKK St.57"
    static let mHavaianasBKKAddress = "No. 6E0 Street 57, Sangkat Beng Keng Kong I, Khan Chamkar Mon, Phnom Penh"
    static let mHavaianasBKKCoordinate = CLLocationCoordinate2D(latitude: 11.554626, longitude: 104.924831)
    
    static let mHavaianasTKAvenueTitle = "HAVAIANAS TK Avenue"
    static let mHavaianasTKAvenueAddress = "No. 80, St. 315, Corner of St. 516 Khan Toul Kork, Phnom Penh"
    static let mHavaianasTKAvenueCoordinate = CLLocationCoordinate2D(latitude: 11.583358, longitude: 104.899132)
    
    static let mHavaianasKingsRoadAngkorTitle = "HAVAIANAS King's Road Angkor"
    static let mHavaianasKingsRoadAngkorAddress = "Corner of 7 Makara & Acha Sva Street, Sangkat Sala Komroek, Siem Reap"
    static let mHavaianasKingsRoadAngkorCoordinate = CLLocationCoordinate2D(latitude: 13.366616, longitude: 103.860176)
    
    static let mHavaianasLuckyMallTitle = "HAVAIANAS Lucky Mall"
    static let mHavaianasLuckyMallAddress = "Sivutha Street, E1-02, Sangkat Svay Dangkum, Siem Reap"
    static let mHavaianasLuckyMallCoordinate = CLLocationCoordinate2D(latitude: 13.362131, longitude: 103.855362)
    
    static let mHavaianasAngkorTicketCenterTitle = "HAVAIANAS Angkor Ticket Center"
    static let mHavaianasAngkorTicketCenterAddress = "St. 60, Trang Village, Sangkat Slor Kram, Siem Reap"
    static let mHavaianasAngkorTicketCenterCoordinate = CLLocationCoordinate2D(latitude: 13.377178, longitude: 103.880508)
    
    static let mHavaianasExchangeSquareMallTitle = "HAVAIANAS Exchange Square Mall"
    static let mHavaianasExchangeSquareMallAddress = "#28 St. 106, Corner St. 51, Daun Penh, Phnom Penh"
    static let mHavaianasExchangeSquareMallCoordinate = CLLocationCoordinate2D(latitude: 11.573926, longitude: 104.921361)
    
    static let mHavaianasSoryaCenterPointTitle = "HAVAIANAS Sorya Center Point"
    static let mHavaianasSoryaCenterPointAddress = "#13-61, St. 63, Sangkat Phsar Thmei I, Khan Daun Penh, Phnom Penh"
    static let mHavaianasSoryaCenterPointCoordinate = CLLocationCoordinate2D(latitude: 11.567241, longitude: 104.920895)
    
    static let mHavaianasPhnomPenhAirportTitle = "HAVAIANAS Phnom Penh Airport"
    static let mHavaianasPhnomPenhAirportAddress = "12000 Angkor Phnom Penh St, Phnom Penh"
    static let mHavaianasPhnomPenhAirportCoordinate = CLLocationCoordinate2D(latitude: 11.552779, longitude: 104.845058)
    
    static let mHavaianasAEONMallSenSokTitle = "HAVAIANAS AEON Mall Sen Sok City"
    static let mHavaianasAEONMallSenSokAddress = "AEON Mall Sen Sok City"
    static let mHavaianasAEONMallSenSokCoordinate = CLLocationCoordinate2D(latitude: 11.600647, longitude: 104.885638)
    
    //INORI
    static let mINoRiJewels = "INORI"
    static let mINoRiJewelsAEONMallSenSokTitle = "INORI AEON Mall Sen Sok City"
    static let mINoRiJewelsAEONMallSenSokAddress = "AEON Mall Sen Sok City, 1st Floor, St. 1003 Village Bayab, Sangkat Phnom Penh Thmey, Khan Sen Sok, Phnom Penh, Cambodia"
    static let mINoRiJewelsCoordinate = CLLocationCoordinate2D(latitude: 11.5996873, longitude: 104.8832553)
    
    //METALLURGY
    static let mMetallurgy = "METALLURGY"
    static let mMetallurgyAEONMallTitle = "METALLURGY AEON Mall Phnom Penh"
    static let mMetallurgyAEONMallAddress = "No. 132 Sothearos Blvd. Unit #0013 (Located on the 1st Floor), Phnom Penh"
    static let mMetallurgyAEONMallCoordinate = CLLocationCoordinate2D(latitude: 11.5997746, longitude: 104.8504244)
    
    static let mMetallurgyTKAvenueTitle = "METALLURGY TK Avenue"
    static let mMetallurgyTKAvenueAddress = "No. 80, St. 315, Corner of St. 516, Khan Toul Kork, Phnom Penh"
    static let mMetallurgyTKAvenueCoordinate = CLLocationCoordinate2D(latitude: 11.5838252, longitude: 104.896952)
    
    //OXYGEN
    static let mOxygen = "OXYGEN"
    static let mOxygensAEONMallSenSokTitle = "OXYGEN AEON Mall Sen Sok City"
    static let mOxygenAEONMallSenSokAddress = "AEON Mall Sen Sok City, 1st Floor, St. 1003 Village Bayab, Sangkat Phnom Penh Thmey, Khan Sen Sok, Phnom Penh, Cambodia"
    static let mOxygenAEONMallSenSokCoordinate = CLLocationCoordinate2D(latitude: 11.5996873, longitude: 104.8832553)
    
    //BONCHON
    static let mBonchon = "BONCHON"
    static let mBonchonPhnomPenhCenterTitle = "BONCHON Phnom Penh Center"
    static let mBonchonPhnomPenhCenterAddress = "Phnom Penh Center, Sothearos Blvd. (Near Build Bright University) Phnom Penh Cambodia"
    static let mBonchonPhnomPenhCenterCoordinate = CLLocationCoordinate2D(latitude: 11.555328, longitude: 104.930673)
    
    static let mBonchonAeonMallTitle = "BONCHON Aeon Mall"
    static let mBonchonAeonMallAddress = "No. 132 Sothearos Blvd. Unit #0013 (Located on the Ground Floor), Phnom Penh"
    static let mBonchonAeonMallCoordinate = CLLocationCoordinate2D(latitude: 11.5997746, longitude: 104.8504244)
    
    static let mBonchonBKKTitle = "BONCHON BKK"
    static let mBonchonBKKAddress = "No. 80, Sangkat Beung Kak 1, Khan Toul Kork, Phnom Penh, Cambodia"
    static let mBonchonBKKCoordinate = CLLocationCoordinate2D(latitude: 11.5833789, longitude: 104.8968343)
    
    static let mBonchonRiverSideTitle = "BONCHON River Side"
    static let mBonchonRiverSideAddress = "Riverside, No. 386 Theamak Lethet Ouk (184) Phnom Penh, Cambodia"
    static let mBonchonRiverSideCoordinate = CLLocationCoordinate2D(latitude: 11.5659663, longitude: 104.9297762)
    
    static let mBonchonTKTitle = "BONCHON TK"
    static let mBonchonTKAddress = "No. 80, Sangkat Beung Kak 1, Khan Toul Kork, Phnom Penh, Cambodia"
    static let mBonchonTKCoordinate = CLLocationCoordinate2D(latitude: 11.5833789, longitude: 104.8968343)
    
    static let mBonchonAeonMaxValueExpressTitle = "BONCHON AEON MaxValu Express"
    static let mBonchonAeonMaxValueExpressAddress = "No. 79, Street 315, Phum 6, Sangkat Boeung Kok 2, Khan Toul Kork, Phnom Penh, Cambodia"
    static let mBonchonAeonMaxValueExpressCoordinate = CLLocationCoordinate2D(latitude: 11.5749106, longitude: 104.8822669)
    
    static let mBonchonExchangeSquareTitle = "BONCHON Exchange Square"
    static let mBonchonExchangeSquareAddress = "#S3-01, Second Floor, St. 51-61 and St. 102-106, Sangkat Wat Phnom, Khan Daun Penh, Phnom Penh"
    static let mBonchonExchangeSquareCoordinate = CLLocationCoordinate2D(latitude: 11.5740449, longitude: 104.9187677)
    
    static let mBonchonIFLTitle = "BONCHON IFL"
    static let mBonchonIFLAddress = "No. 120C1, St. 110 (Russian Blvd.) Sangkat Tek Laak 1, Khan Toul Kork, Phnom Penh, Cambodia"
    static let mBonchonIFLCoordinate = CLLocationCoordinate2D(latitude: 11.5679605, longitude: 104.8915755)
    
    static let mBonchonAttwoodTitle = "BONCHON Attwood"
    static let mBonchonAttwoodAddress = "Unit No. 15 #31E0, Russian Blvd, Sangkat Teok Thla, Khan Sen Sok, Phnom Penh, Cambodia"
    static let mBonchonAttwoodCoordinate = CLLocationCoordinate2D(latitude: 11.5620619, longitude: 104.8698257)
    
    static let mBonchonSovannaTitle = "BONCHON Sovanna"
    static let mBonchonSovannaAddress = "Sovanna Shopping Center. Ground floor, Unit S1-02/06/08. #307-309, Street 271, Sangkat Tumnob Teuk, Khan Chamkar Morn, Phnom Penh, Cambodia"
    static let mBonchonSovannaCoordinate = CLLocationCoordinate2D(latitude: 11.5412539, longitude: 104.8803398)
    
    static let mBonchonStoengMeancheyTitle = "BONCHON Stoeng Meanchey"
    static let mBonchonStoengMeancheyAddress = "Lot 0013-2 #1024AE0, Street 217 Sangkat Stung Meanchey, Khan Meanchey Phnom Penh, Cambodia"
    static let mBonchonStoengMeancheyCoordinate = CLLocationCoordinate2D(latitude: 11.5413369, longitude: 104.8803398)
    
    static let mBonchonSoryaMallTitle = "BONCHON Sorya Mall"
    static let mBonchonSoryaMallAddress = "Ground floor. Unit FG-F07-08, #13-61 Street 63, Sangkat Phsar Thmey I Khan Daun Penh, Phnom Penh, Cambodia"
    static let mBonchonSoryaMallCoordinate = CLLocationCoordinate2D(latitude: 11.54142, longitude: 104.8803398)
    
    static let mBonchonAeonMallSenSokCityTitle = "BONCHON AEON Mall Sen Sok City"
    static let mBonchonAeonMallSenSokCityAddress = "AEON Mall Sen Sok City, 2nd Floor, St. 1003 Village Bayab Sangkat Phnom Penh Thmey, Khan Sen Sok, Phnom Penh, Cambodia"
    static let mBonchonAeonMallSenSokCityCoordinate = CLLocationCoordinate2D(latitude: 11.5996873, longitude: 104.8832553)
    
    //SPRAYWAY
    static let mSprayWay = "SPRAYWAY"
    static let mSprayWayAEONMallSenSokTitle = "SPRAYWAY AEON Mall Sen Sok City"
    static let mSprayWayAEONMallSenSokAddress = "AEON Mall Sen Sok City, 1st Floor, St. 1003 Village Bayab, Sangkat Phnom Penh Thmey, Khan Sen Sok, Phnom Penh, Cambodia"
    static let mSprayWayAEONMallSenSokCoordinate = CLLocationCoordinate2D(latitude: 11.5996873, longitude: 104.8832553)
    
    //SAN FRANCISCO CAFFE
    static let mSanFrancisco = "SANFRANCISCO"
    static let mSanFranciscoExchangeSquareTitle = "SANFRANCISCO CAFFE Exchange Square"
    static let mSanFranciscoExchangeSquareAddress = "No. 28, Street 106 (Corner Street 51), Sangkat Wat Phnom, Khan Duan Penh, Phnom Penh, Cambodia"
    static let mSanFranciscoExchangeSquareCoordinate = CLLocationCoordinate2D(latitude: 11.574024, longitude: 104.921338)
    
    static let mSanFranciscoAEONMallSenSokTitle = "SANFRANCISCO CAFFE AEON Mall Sen Sok City"
    static let mSanFranciscoAEONMallSenSokAddress = "AEON Mall Sen Sok City, Ground Floor, St. 1003 Village Bayab, Sangkat Phnom Penh Thmey, Khan Sen Sok, Phnom Penh, Cambodia."
    static let mSanFranciscoAEONMallSenSokCoordinate = CLLocationCoordinate2D(latitude: 11.5996873, longitude: 104.8832553)
    
    //ROKKU
    static let mRokku = "Rokku"
    static let mRokkuTitle = "Rokku Sushi Lounge and Bistro"
    static let mRokkuAddress = "No. 507 Sisowath Quay Corner of Suramarit Blvd. Phnom Penh, Cambodia"
    static let mRokkuCoordinate = CLLocationCoordinate2D(latitude: 11.5570174, longitude: 104.9313678)
    
    //outlet Pin
    static let mBirkenStockPin = "birkenstockPin"
    static let mBonchonPin = "bonchonPin"
    static let mCrocsPin = "crocsPin"
    static let mForMePin = "formePin"
    static let mHauteRackPin = "hauterackPin"
    static let mHavaianasPin = "havaianasPin"
    static let mInoriPin = "inoriPin"
    static let mMetallurgyPin = "metallurgyPin"
    static let mOxygenPin = "oxygenPin"
    static let mPenshoppePin = "penshoppePin"
    static let mRokkuPin = "rokkuPin"
    static let mSanFranciscoPin = "san_francisco_cafePin"
    static let mSprayWayPin = "spraywayPin"
    static let mSwatchPin = "swatchPin"
    
    //Xilnex API
    static let urlXilnexSearched = "https://api.xilnex.com/logic/v2/clients/search?code="
    static let urlXilnexCreateNewCustomer = "https://api.xilnex.com/logic/v2/clients/client"
    
    //Iter Plus API
    static let urlIterPlusRequestToken = "http://96.9.80.84:8181/wingBridge/api/request/token"
    static let urlIterPlusVertifyMasterCard = "http://96.9.80.84:8181/wingBridge/api/request/card/number"

    //other
    static let mXilnexFBpage = "https://www.facebook.com/XilnexCambodia/"
    static let mXilnexContactNumber = "081666058"
    static let NOT_SIGN_IN = ""
    static let SIGN_IN = "sign in"
    static let SYNC = "sync"
    
    //Xilnex Token
    static let mSohoToken = "24U3HurDqGbf1oi7nZZTsYkIQDEk2ZYP48x/TWAJ004="
    //Xilnex Dkey
    static let mSohoDkey = "VDEHwLkcwg6pi1P4GUekHpbc4hB8Oh7C"
    
    //Outlet card, logo and name
    static let mSohoCardName = "card_soho"
    static let mSohoLogoName = "logo_soho"
    static let mSoho = "Soho"
    
    //Soho website
    static let mSohoWebsite = "http://www.soho.com.kh/about-us/"
    
    static let PROMO_SWATCH = "pro_swatch"
    static let PROMO_BIRKENSTOCK = "pro_birkenstock"
    static let PROMO_OXGN = "pro_oxgn"
    static let PROMO_METALLURGY = "pro_metallurgy"
    static let PROMO_FORME = "pro_forme"
    static let PROMO_INORI = "pro_inori"
    static let PROMO_BONCHON = "pro_bonchon"
    static let PROMO_CROCS = "pro_crocs"
    static let PROMO_HAUTERACK = "pro_hauterack"
    static let PROMO_HAVAIANAS = "pro_havaianas"
    static let PROMO_PENSHOPPE = "pro_penshoppe"
    static let PROMO_ROKKU = "pro_rokku"
    static let PROMO_SANFRANCISCO = "pro_sanfrancisco"
    static let PROMO_SPRAYWAY = "pro_sprayway"
    
    //SOHO promotion website
    static let mPromoBirkenstock = "http://www.demo.sohoclub.com.kh/brands/birkenstock"
    static let mPromoBonchon = "http://www.demo.sohoclub.com.kh/brands/bonchon"
    static let mPromoCrocs = "http://www.demo.sohoclub.com.kh/brands/crocs"
    static let mPromoForme = "http://www.demo.sohoclub.com.kh/brands/forme"
    static let mPromoHauteRack = "http://www.demo.sohoclub.com.kh/brands/haute-rack"
    static let mPromoHavaianas = "http://www.demo.sohoclub.com.kh/brands/havaianas"
    static let mPromoInori = "http://www.demo.sohoclub.com.kh/brands/inori-jewels"
    static let mPromoMetallurgy = "http://www.demo.sohoclub.com.kh/brands/metallurgy"
    static let mPromoOxygen = "http://www.demo.sohoclub.com.kh/brands/oxygen"
    static let mPromoPenshoppe = "http://www.demo.sohoclub.com.kh/brands/penshoppe"
    static let mPromoRokku = "http://www.demo.sohoclub.com.kh/brands/rokku"
    static let mPromoSanFrancisco = "http://www.demo.sohoclub.com.kh/brands/san-francisco-caffe"
    static let mPromoSprayWay = "http://www.demo.sohoclub.com.kh/brands/sprayway"
    static let mPromoSwatch = "http://www.demo.sohoclub.com.kh/brands/swatch"
    
    //Message
    static let messageTitle = "Alert"
    static let phoneNumberInvalid = "Phone number invalided"
    static let registeredCard = "This card already registered"
    static let mSrolanhSmartStoreMessage = "Can not add the card when the point is below 100 point"
    static let mDBNull = "Has no recent history"
    static let loginFailedSMS = "Invalid email or password"
    static let loginNullSMS = "Please fill email and password to login"
    static let registerFailedSMS = "Registration unsuccessful"
    static let noInternetConnection = "No internet connection"
    
    // Xilnex API return function
    static let urlXilnexSalesInVoice = "https://api.xilnex.com/logic/v2/sales/"
    static func urlXilnexSaleHistories(id: String) -> String{
        return "https://api.xilnex.com/logic/v2/clients/\(id)/sales"
    }
    
    //Init all soho outlet
    static func outlets() -> [OutletAnnotationModel] {
        
        return [OutletAnnotationModel(title: mHavaianasPhnomPenhAirportTitle, locationName: mHavaianasPhnomPenhAirportAddress, discipline: mHavaianasPin, coordinate: mHavaianasPhnomPenhAirportCoordinate),
            OutletAnnotationModel(title: mHavaianasPP63Title, locationName: mHavaianasPP63Adddress, discipline: mHavaianasPin, coordinate: mHavaianasPP63Coordinate),
            OutletAnnotationModel(title: mHavaianasLuckyMallTitle, locationName: mHavaianasLuckyMallAddress, discipline: mHavaianasPin, coordinate: mHavaianasLuckyMallCoordinate),
            OutletAnnotationModel(title: mHavaianasKingsRoadAngkorTitle, locationName: mHavaianasKingsRoadAngkorAddress, discipline: mHavaianasPin, coordinate: mHavaianasKingsRoadAngkorCoordinate),
            OutletAnnotationModel(title: mHavaianasAngkorTicketCenterTitle, locationName: mHavaianasAngkorTicketCenterAddress, discipline: mHavaianasPin, coordinate: mHavaianasAngkorTicketCenterCoordinate),
            OutletAnnotationModel(title: mHavaianasBKKTitle, locationName: mHavaianasBKKAddress, discipline: mHavaianasPin, coordinate: mHavaianasBKKCoordinate),
            OutletAnnotationModel(title: mHavaianasTKAvenueTitle, locationName: mHavaianasTKAvenueAddress, discipline: mHavaianasPin, coordinate: mHavaianasTKAvenueCoordinate),
            OutletAnnotationModel(title: mHavaianasExchangeSquareMallTitle, locationName: mHavaianasExchangeSquareMallAddress, discipline: mHavaianasPin, coordinate: mHavaianasExchangeSquareMallCoordinate),
            OutletAnnotationModel(title: mHavaianasSoryaCenterPointTitle, locationName: mHavaianasSoryaCenterPointAddress, discipline: mHavaianasPin, coordinate: mHavaianasSoryaCenterPointCoordinate),
            OutletAnnotationModel(title: mHavaianasAEONMallTitle, locationName: mHavaianasAEONMallAdddress, discipline: mHavaianasPin, coordinate: mHavaianasAEONMallCoordinate),
            OutletAnnotationModel(title: mHavaianasAEONMallSenSokTitle, locationName: mHavaianasAEONMallSenSokAddress, discipline: mHavaianasPin, coordinate: mHavaianasAEONMallSenSokCoordinate),
            
            OutletAnnotationModel(title: mPenshoppeTKTitle, locationName: mPenshoppeTKAddress, discipline: mPenshoppePin, coordinate: mPenshoppeTKCoordinate),
            OutletAnnotationModel(title: mPenshoppeAeonMallTitle, locationName: mPenshoppeAeonMallAddress, discipline: mPenshoppePin, coordinate: mPenshoppeAeonMallCoordinate),
            OutletAnnotationModel(title: mPenshoppeLuckyMallTitle, locationName: mPenshoppeLuckyMallAddress, discipline: mPenshoppePin, coordinate: mPenshoppeLuckyMallCoordinate),
            OutletAnnotationModel(title: mPenshoppeAeonMallSenSokTitle, locationName: mPenshoppeAeonMallSenSokAddress, discipline: mPenshoppePin, coordinate: mPenshoppeAeonMallSenSokCoordinate),
           
            OutletAnnotationModel(title: mForMeAeonMallTitle, locationName: mForMeAeonMallAddress, discipline: mForMePin, coordinate: mForMeAeonMallCoordinate),
            
            OutletAnnotationModel(title: mMetallurgyAEONMallTitle, locationName: mMetallurgyAEONMallAddress, discipline: mMetallurgyPin, coordinate: mMetallurgyAEONMallCoordinate),
            OutletAnnotationModel(title: mMetallurgyTKAvenueTitle, locationName: mMetallurgyTKAvenueAddress, discipline: mMetallurgyPin, coordinate: mMetallurgyTKAvenueCoordinate),
           
            OutletAnnotationModel(title: mOxygensAEONMallSenSokTitle, locationName: mOxygenAEONMallSenSokAddress, discipline: mOxygenPin, coordinate: mOxygenAEONMallSenSokCoordinate),
            
            OutletAnnotationModel(title: mBonchonPhnomPenhCenterTitle, locationName: mBonchonPhnomPenhCenterAddress, discipline: mBonchonPin, coordinate: mOxygenAEONMallSenSokCoordinate),
            OutletAnnotationModel(title: mBonchonAeonMallTitle, locationName: mBonchonAeonMallAddress, discipline: mBonchonPin, coordinate: mBonchonAeonMallCoordinate),
            OutletAnnotationModel(title: mBonchonBKKTitle, locationName: mBonchonBKKAddress, discipline: mBonchonPin, coordinate: mBonchonBKKCoordinate),
            OutletAnnotationModel(title: mBonchonRiverSideTitle, locationName: mBonchonRiverSideAddress, discipline: mBonchonPin, coordinate: mBonchonRiverSideCoordinate),
            OutletAnnotationModel(title: mBonchonTKTitle, locationName: mBonchonTKAddress, discipline: mBonchonPin, coordinate: mBonchonTKCoordinate),
            OutletAnnotationModel(title: mBonchonAeonMaxValueExpressTitle, locationName: mBonchonAeonMaxValueExpressAddress, discipline: mBonchonPin, coordinate: mBonchonAeonMaxValueExpressCoordinate),
            OutletAnnotationModel(title: mBonchonExchangeSquareTitle, locationName: mBonchonExchangeSquareAddress, discipline: mBonchonPin, coordinate: mBonchonExchangeSquareCoordinate),
            OutletAnnotationModel(title: mBonchonIFLTitle, locationName: mBonchonIFLAddress, discipline: mBonchonPin, coordinate: mBonchonIFLCoordinate),
            OutletAnnotationModel(title: mBonchonAttwoodTitle, locationName: mBonchonAttwoodAddress, discipline: mBonchonPin, coordinate: mBonchonAttwoodCoordinate),
            OutletAnnotationModel(title: mBonchonSovannaTitle, locationName: mBonchonSovannaAddress, discipline: mBonchonPin, coordinate: mBonchonSovannaCoordinate),
            OutletAnnotationModel(title: mBonchonStoengMeancheyTitle, locationName: mBonchonStoengMeancheyAddress, discipline: mBonchonPin, coordinate: mBonchonStoengMeancheyCoordinate),
            OutletAnnotationModel(title: mBonchonSoryaMallTitle, locationName: mBonchonSoryaMallAddress, discipline: mBonchonPin, coordinate: mBonchonSoryaMallCoordinate),
            OutletAnnotationModel(title: mBonchonAeonMallSenSokCityTitle, locationName: mBonchonAeonMallSenSokCityAddress, discipline: mBonchonPin, coordinate: mBonchonAeonMallSenSokCityCoordinate),
            
            OutletAnnotationModel(title: mSanFranciscoExchangeSquareTitle, locationName: mSanFranciscoExchangeSquareAddress, discipline: mSanFranciscoPin, coordinate: mSanFranciscoExchangeSquareCoordinate),
            OutletAnnotationModel(title: mSanFranciscoAEONMallSenSokTitle, locationName: mSanFranciscoAEONMallSenSokAddress, discipline: mSanFranciscoPin, coordinate: mSanFranciscoAEONMallSenSokCoordinate),
            
            OutletAnnotationModel(title: mCrocsTKAvenueTitle, locationName: mCrocsTKAvenueAddress, discipline: mCrocsPin, coordinate: mCrocsTKAvenueCoordinate),
            OutletAnnotationModel(title: mCrocsExchangeSquareTitle, locationName: mCrocsExchangeSquareAddress, discipline: mCrocsPin, coordinate: mCrocsExchangeSquareCoordinate),
            OutletAnnotationModel(title: mCrocsAeonMallTitle, locationName: mCrocsAeonMallAddress, discipline: mCrocsPin, coordinate: mCrocsAeonMallCoordinate),
            OutletAnnotationModel(title: mCrocsSoryaMallTitle, locationName: mCrocsSoryaMallAddress, discipline: mCrocsPin, coordinate: mCrocsSoryaMallCoordinate),
            OutletAnnotationModel(title: mCrocsSihanoukBlvdTitle, locationName: mCrocsSihanoukBlvdAddress, discipline: mCrocsPin, coordinate: mCrocsSihanoukBlvdCoordinate),
            OutletAnnotationModel(title: mCrocsAEONMallSenSokTitle, locationName: mCrocsAEONMallSenSokAddress, discipline: mCrocsPin, coordinate: mCrocsAEONMallSenSokCoordinate),
            
            OutletAnnotationModel(title: mSwatchAeonMallTitle, locationName: mSwatchAeonMallAddress, discipline: mSwatchPin, coordinate: mSwatchAeonMallCoordinate),
            OutletAnnotationModel(title: mSwatchExchangeSquareTitle, locationName: mSwatchExchangeSquareAddress, discipline: mSwatchPin, coordinate: mSwatchExchangeSquareCoordinate),
            OutletAnnotationModel(title: mSwatchAEONMallSenSokTitle, locationName: mSwatchAEONMallSenSokAddress, discipline: mSwatchPin, coordinate: mSwatchAEONMallSenSokCoordinate),
            
            OutletAnnotationModel(title: mRokkuTitle, locationName: mRokkuAddress, discipline: mRokkuPin, coordinate: mRokkuCoordinate),
            OutletAnnotationModel(title: mINoRiJewelsAEONMallSenSokTitle, locationName: mINoRiJewelsAEONMallSenSokAddress, discipline: mInoriPin, coordinate: mINoRiJewelsCoordinate),
            OutletAnnotationModel(title: mSprayWayAEONMallSenSokTitle, locationName: mSprayWayAEONMallSenSokAddress, discipline: mSprayWayPin, coordinate: mSprayWayAEONMallSenSokCoordinate),
            OutletAnnotationModel(title: mBirkenstockAEONMallSenSokTitle, locationName: mBirkenstockAEONMallSenSokAddress, discipline: mBirkenStockPin, coordinate: mBirkenstockAEONMallSenSokCoordinate),
            OutletAnnotationModel(title: mHauteRackAeonSenSokTitle, locationName: mHauteRackAeonSenSokAddress, discipline: mHauteRackPin, coordinate: mHauteRackAeonSenSokCoordinate)
        ]
    }
    
    //MARK: save isCheckSignUp
    static func saveIsCheckSignUp(check: String){
        
        let contxt = Context.getContext()
        let entity = NSEntityDescription.entity(forEntityName: "LoginToken", in: contxt)
        let manageObj = NSManagedObject(entity: entity!, insertInto: contxt)
        manageObj.setValue(check, forKey: "token")
        do{
            try contxt.save()
        } catch{
            print("error")
        }
    }
    
    //MARK: save isCheck Sync
    static func saveIsCheckSync(check: String){
        
        let contxt = Context.getContext()
        let entity = NSEntityDescription.entity(forEntityName: "Sync", in: contxt)
        let manageObj = NSManagedObject(entity: entity!, insertInto: contxt)
        manageObj.setValue(check, forKey: "sync")
        do{
            try contxt.save()
        } catch{
            print("error")
        }
    }
    
    //MARK: Fetch Sync
    static func fetchSync() -> String{
        var s: String = ""
        let fetchRequest: NSFetchRequest<Sync> = Sync.fetchRequest()
        
        do {
            let fetchResults = try Context.getContext().fetch(fetchRequest)
            for item in fetchResults {
                s = item.sync!
            }
        } catch {
            print("Error with request: \(error)")
        }
        return s
    }
    
    //MARK: Delete IsCheck Sync
    static func deleteIsCheckSync(){
        let contxt = Context.getContext()
        let fetchRequest: NSFetchRequest<Sync> = Sync.fetchRequest()
        if let result = try? contxt.fetch(fetchRequest) {
            for object in result {
                contxt.delete(object)
                do{
                    try contxt.save()
                } catch{
                    print("error")
                }
            }
        }
    }
    
    //MARK: save card
    static func saveData(with frontData: NSData, backData: NSData, code: String){
        
        let conText = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "Card", in: conText)
        let manageObj = NSManagedObject(entity: entity!, insertInto: conText)
        manageObj.setValue(frontData, forKey: "imagedata")
        manageObj.setValue(backData, forKey: "imagedata2")
        manageObj.setValue(code, forKey: "barcode")
        do{
            if code != ""{
                try conText.save()
            }
        } catch{
            print("error")
        }
        
    }
    
    // MARK:
    static func isCheckMember(serial: String) -> Bool {
        var isHave: Bool! = false
        let fetchRequest: NSFetchRequest<Card> = Card.fetchRequest()
        
        do {
            let fetchResults = try Context.getContext().fetch(fetchRequest)
            for item in fetchResults {
                if item.serial == serial{
                    isHave = true
                    return isHave == true
                }
            }
        } catch {
            print("Error with request: \(error)")
        }
        return isHave
    }
    
    //MARK: save card with membership
    static func saveMemberCard(serial: String ,name: String,id: String, dob: String, phone: String, point: String, email: String, outlet: String, logo: String, token: String, dkey: String){
        DispatchQueue.main.async {
            let conText = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
            let entity = NSEntityDescription.entity(forEntityName: "Card", in: conText)
            let manageObj = NSManagedObject(entity: entity!, insertInto: conText)
            
            manageObj.setValue(serial, forKey: "serial")
            manageObj.setValue(name, forKey: "name")
            manageObj.setValue(id, forKey: "id")
            manageObj.setValue(point, forKey: "point")
            manageObj.setValue(email, forKey: "email")
            manageObj.setValue(phone, forKey: "phone")
            manageObj.setValue(dob, forKey: "dob")
            manageObj.setValue(logo, forKey: "logo")
            manageObj.setValue(outlet, forKey: "outlet")
            manageObj.setValue(token, forKey: "token")
            manageObj.setValue(dkey, forKey: "dkey")
            do{
                try conText.save()
                Ulti.subStoryBoard()
            } catch{
                print("Could not load data from database \(error.localizedDescription)")
            }
        }
    }
    
    //MARK: Fetch Sign Up
    static func fetchSignUp() -> String{
        var s: String = ""
        let fetchRequest: NSFetchRequest<LoginToken> = LoginToken.fetchRequest()
        
        do {
            let fetchResults = try Context.getContext().fetch(fetchRequest)
            for item in fetchResults {
                s = item.token!
            }
        } catch {
            print("Error with request: \(error)")
        }
        return s
    }

    //MARK: Delete IsCheckSignUp
    static func deleteIsCheckSignUp(){
        let contxt = Context.getContext()
        let fetchRequest: NSFetchRequest<LoginToken> = LoginToken.fetchRequest()
        if let result = try? contxt.fetch(fetchRequest) {
            for object in result {
                contxt.delete(object)
                do{
                    try contxt.save()
                } catch{
                    print("error")
                }
            }
        }
    }
    
    //MARK: Delete all card
    static func deleteAllCard(){
        let contxt = Context.getContext()
        let fetchRequest: NSFetchRequest<Card> = Card.fetchRequest()
        if let result = try? contxt.fetch(fetchRequest) {
            for object in result {
                contxt.delete(object)
                do{
                    try contxt.save()
                } catch{
                    print("error")
                }
            }
        }
    }

    //MARK: Update data
    static func updateData(cardNo: String, cardName: String, mobile: String, point: String){
        let context = Context.getContext()
        let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest.init(entityName: "Card")
        let predicate = NSPredicate(format: "serial = '\(cardNo)'")
        fetchRequest.predicate = predicate
        do
        {
            let test = try context.fetch(fetchRequest)
            if test.count == 1
            {
                let objectUpdate = test[0] as! NSManagedObject
                objectUpdate.setValue(cardName, forKey: "name")
                objectUpdate.setValue(mobile, forKey: "phone")
                objectUpdate.setValue(point, forKey: "point")
                do{
                    try context.save()
                }
                catch
                {
                    print(error)
                }
            }
        }
        catch
        {
            print(error)
        }
    }
    
    static func isInternetAvailable() -> Bool{
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        return (isReachable && !needsConnection)
    }
    
    static func checkConnection(connectionView: UIView){
        let connection = isInternetAvailable()
        if (connection){
            connectionView.isHidden = true
        }else{
            connectionView.isHidden = false
        }
    }
    
    static func uploadDeviceToken(token: String){
        let postData = NSMutableData(data: "token=\(token)".data(using: String.Encoding.utf8)!)
        
        let request = NSMutableURLRequest(url: NSURL(string: "http://96.9.80.84:8181/api/notification")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.httpBody = postData as Data
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print(error!)
            } else {
                let httpResponse = response as? HTTPURLResponse
                print(httpResponse!)
            }
        })
        
        dataTask.resume()
    }
    
    //MARK: Show Message
    static func showMessage(title: String, message: String, actionTitle: String) -> UIAlertController{
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: actionTitle, style: UIAlertAction.Style.cancel, handler: nil))
        return alert
    }
    
    static func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }

    //MARK: set SlideShow
    static func slideShow(scrollView: UIScrollView , position: slidePosition , imageName: String)-> UIView{
        
        scrollView.frame = CGRect(x: 0, y: 0, width: position.w, height: position.h)
        let image = UIImageView(frame: CGRect(x: position.x ,y: position.y ,width: position.w, height: position.h))
        
        image.contentMode = .scaleAspectFill
        image.image = UIImage(named: imageName)
        
        return image
    }
    
    struct slidePosition{
        var x: CGFloat
        var y: CGFloat
        var w: CGFloat
        var h: CGFloat
        
        init(x: CGFloat , y: CGFloat , w: CGFloat , h: CGFloat) {
            self.x = x
            self.y = y
            self.w = w
            self.h = h
        }
    }
    
    //MARKL check iphone X
    static func isIphoneX() -> Bool{
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 2436:
                return true
            default:
                print("unknown")
            }
        }
        return false
    }
    
    //Remove dubliicateItem
    static func removeDuplicates(_ nums: inout [Int]) -> Int {
        
        guard let first = nums.first else {
            return 0 // Empty array
        }
        var current = first
        var count = 1
        
        for elem in nums.dropFirst() {
            if elem != current {
                nums[count] = elem
                current = elem
                count += 1
            }
        }
        
        nums.removeLast(nums.count - count)
        return count
    }
    
    static func qrCodeGenarator(string: String) -> UIImage?{
        let data = string.data(using: String.Encoding.ascii)
        
        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 3, y: 3)
            
            if let output = filter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
        }
        
        return nil
    }
    
    static func subStoryBoard(){
        let vc = UIStoryboard(name: "Sub", bundle:nil).instantiateViewController(withIdentifier: "SubController") as UIViewController
        let appDelegate = (UIApplication.shared.delegate as! AppDelegate)
        appDelegate.window?.rootViewController = vc
    }
}

