//
//  GenderPopUpVC.swift
//  soho
//
//  Created by WML on 8/17/18.
//  Copyright © 2018 sohogroup. All rights reserved.
//

import UIKit

protocol GProtocol {
    func didSelectedGender(gender: String)
}

class GenderPopUpVC: UIViewController {

    @IBOutlet weak var genderPickerView: UIPickerView!
    let gender = ["Male", "Female"]
    var delegate: GProtocol?
    var selectedGender = "Male"
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func CancelAction(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func doneAction(_ sender: UIButton) {
        if delegate != nil {
            delegate?.didSelectedGender(gender: selectedGender)
            dismiss(animated: true, completion: nil)
        }
    }
}



extension GenderPopUpVC: UIPickerViewDelegate, UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    
    // The number of rows of data
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return gender.count
    }
    
    // The data to return for the row and component (column) that's being passed in
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return gender[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedGender = gender[row]
        print(gender[row])
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let attributedString = NSAttributedString(string: gender[row], attributes: [NSAttributedString.Key.foregroundColor: UIColor(rgb: 0x898A95)])
        
        return attributedString
    }
}
