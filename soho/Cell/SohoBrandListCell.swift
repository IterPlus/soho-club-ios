//
//  SohoBrandListVC.swift
//  soho
//
//  Created by WML on 5/18/18.
//  Copyright © 2018 sohogroup. All rights reserved.
//

import UIKit

class SohoBrandListCell: UITableViewCell {

    @IBOutlet weak var sohoBrandImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
