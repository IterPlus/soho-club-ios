//
//  PromotionMenuCell.swift
//  soho
//
//  Created by WML on 9/21/18.
//  Copyright © 2018 sohogroup. All rights reserved.
//

import UIKit

class PromotionMenuCell: UICollectionViewCell {
    
    @IBOutlet weak var promotionImage: UIImageView!
}
