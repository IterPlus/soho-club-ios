//
//  SaleHistoriesCell.swift
//  soho
//
//  Created by WML on 5/10/18.
//  Copyright © 2018 sohogroup. All rights reserved.
//

import UIKit

class SaleHistoriesCell: UITableViewCell {

    @IBOutlet weak var outlet: UILabel!
    @IBOutlet weak var saleIDLabel: UILabel!
    @IBOutlet weak var saleDate: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var paidAmountLabel: UILabel!
    @IBOutlet weak var detailButton: UIButton!
    @IBOutlet weak var containerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
